import React, { Component } from 'react';
import Navbar from './components/Navbar';
import Main from './components/Main';
import './App.css';
import Wrapper from './components/Wrapper';
import Footer from './components/Footer';
import Reserved from './components/Reserved';

class App extends Component {
  render() {
    return (
      <div className="App">
      
       <Navbar
       logo="ProgFun"/>

       <Main 
       tagline="Coding is fun more than you Think!"
       values = "CREATE - DESIGN - CODE"
       codersdesc="I think most programmers spend first 5 years of their
       career mastering complexity and the rest of their lives learning simplicity"/>
       
        <Wrapper
        title="Design" 
        desc ="Built for & ny nerds"
        description ="Like you love coding just enjoy your
        career whith something good and improve your skills not limited"
        recent_title ="Recent Work"
        recent_description= "Codecademy is an online freemium interactive platform 
        that offers free coding classes in 12 different programming languages 
        including Python, Java, JavaScript, 
        Ruby, SQL, and Sass, as well as markup languages HTML and CSS. Wikipedia"
        catalogue_title= "Catalogue"
        catalogue_desc= "Including Python, Java, JavaScript, 
        Ruby, SQL, and Sass, as well as markup languages HTML and CSS. Wikipedia"
        button ="View Profile"/>
        <Footer 
        title ="GET IN TOUCH"
        desc="We're social and we'd like to hear from you!
        Feel free to send us an email, find us on Google Plus, follow us on Twitter and Join
        us on Facebook"/>
        <Reserved 
        reserved="ProgFun @2018 - All rights reserved. Developed by Brightely and Rigoeffector"/>

      </div>
    );
  }
}

export default App;
