 import React, { Component } from 'react';
 import '../App';

class Footer extends Component {
    render() {
        return (
            <div className="Footer">
                <div className="Intouch">
                    <h2>{this.props.title}</h2>
                    <p>{this.props.desc}</p>
                    <div className="input">
                        <input type="text" placeholder="Subscribe to our newsletter" className="txt-subscribe"/>
                        <button className=".btn-subscribe"> SUBSCRIBE </button>
                    </div>
                    <div className="social_Media">
                    <i id= "icons_social" class="fab fa-twitter"></i>
                    <i id= "icons_social" class="fab fa-facebook-f"></i>
                    <i id= "icons_social" class="fab fa-linkedin-in"></i>
                    <i id= "icons_social" class="fab fa-google-plus-g"></i>
                    </div>
                </div>
            </div>
            
        );
    }
}

export default Footer;