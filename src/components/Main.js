import React, { Component } from 'react';
import '../App';

class Main extends Component {
    render() {
        return (
            <div>
            <div className="main-header">
            <div className="main-header-overlay">
                    {/* <div className="header-settings">
                    <i id="setting" class="fas fa-cogs"></i>
                    </div> */}
                    <div className="header-content">
                        <h1> {this.props.tagline} </h1>
                        <h3> {this.props.values} </h3>
                        <button className="btn-portfolio"> OUR PORTFOLIO </button>
                    </div>
                    </div>
            </div>
            <section>
                    <div className = "coders-duty">
                        <h3>{this.props.codersdesc}</h3>
                    </div> 
                </section>
            </div>
        );
    }
}

export default Main;