 import React, { Component } from 'react';
 import '../App';

class Navbar extends Component {
    render() {
        var pages = ['Home', 'Pages', 'Work', 'Style', 'Extras','Blog', 'Contact'];
        var pagesLinks = pages.map(function(page){
            return (
                <a href = {"/" +page}>
                    {page}
                </a>
            );
        });
        return (
            <div className="navbar">
                <div className= "nav-logo">
                   <h1>{this.props.logo}</h1>
                </div>
                <div className = "nav-links">
                    <nav>{pagesLinks}</nav>
                </div>
            </div>
        );
    }
}

export default Navbar;