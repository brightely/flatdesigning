import React, { Component } from 'react';
import '../App';

class Reserved extends Component {
   render() {
       return (      
               <div className="Reserved_all">
                   <p>{this.props.reserved}</p>
               </div>    
       );
   }
}

export default Reserved;