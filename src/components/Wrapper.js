 import React, { Component } from 'react';
import '../App';
class Wrapper extends Component {
    render() {
        return (
            <div className="Wrapper">
                <div className="flex">
                    <div className="flex_box">
                        <div id="flex_icon">
                        <i id="icon" class="fas fa-pencil-alt"></i>
                        </div>
                        <div className="flex_title">
                            <p>{this.props.title}</p>
                            <h2>{this.props.desc}</h2>
                        </div>
                        <div className="flex_description">
                        <p>{this.props.description}</p>
                        </div>
                    </div>
                    {/* end of flex box */}

                    <div className="flex_box">
                        <div id="flex_icon">
                        <i id="icon"  class="fas fa-umbrella"></i>
                        </div>
                        <div className="flex_title">
                            <p>{this.props.title}</p>
                            <h2>{this.props.desc}</h2>
                        </div>
                        <div className="flex_description">
                        <p>{this.props.description}</p>
                        </div>
                    </div>
                    {/* end of flex box */}


                    <div className="flex_box">
                        <div id="flex_icon">
                        <i id="icon"  class="fas fa-book"/>
                        </div>
                        <div className="flex_title">
                            <p>{this.props.title}</p>
                            <h2>{this.props.desc}</h2>
                        </div>
                        <div className="flex_description">
                        <p>{this.props.description}</p>
                        </div>
                    </div>
                    {/* end of flex box */}



                    <div className="flex_box">
                        <div id="flex_icon">
                        <i id="icon"  class="fab fa-magento"></i>
                        </div>
                        <div className="flex_title">
                            <p>{this.props.title}</p>
                            <h2>{this.props.desc}</h2>
                        </div>
                        <div className="flex_description">
                        <p>{this.props.description}</p>
                        </div>
                    </div>
                    {/* end of flex box */}
                   
                </div>
                {/* end of flex class */}

 {/* recent works down of flex boxes */}
                <div className="Recent_work">
                    <div className="recent_flex">
                        <div className="recent_flexBox">
                            <div className="recent_title">
                                <p>{this.props.recent_title}</p>

                            </div>
                            <div className="recent_paragraph">
                            <p>{this.props.recent_description}</p>
                            </div>
                            <button>{this.props.button}</button>

                        </div>
                        
                    </div>
               

                {/* other side of flexing box */}
                <div className="catalogue">
                    <div className="images_catalogue">
                    <img src={require('../images/1.jpg')}/>
                    </div>
                    <div className="ctatlogue_desc">
                        <h2>{this.props.catalogue_title}</h2>
                        <p>{this.props.catalogue_desc}</p>
                    </div>
                </div>
              

                 <div className="catalogue">
                    <div className="images_catalogue">
                    <img src={require('../images/1.jpg')}/>
                    </div>
                    <div className="ctatlogue_desc">
                        <h2>{this.props.catalogue_title}</h2>
                        <p>{this.props.catalogue_desc}</p>
                    </div>
                </div>
                <div className="catalogue">
                    <div className="images_catalogue">
                    <img src={require('../images/1.jpg')}/>
                    </div>
                    <div className="ctatlogue_desc">
                        <h2>{this.props.catalogue_title}</h2>
                        <p>{this.props.catalogue_desc}</p>
                    </div>
                </div>
            </div>
            </div>
            // end of wrapper
        );
    }
}

export default Wrapper;